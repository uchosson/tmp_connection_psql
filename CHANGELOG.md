# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2023-01-20

### Added

- Add the function `connect()` to `__init__.py`.

## [1.2.0] - 2022-11-18

### Changed

- Update pytest, pytest-sugar and mypy.

## [1.1.0] - 2022-11-04

### Added

- tmp_connection_psql now support python3.11.
- Add new linter pep8-naming, following remove flakehell which is no longer maintained.

### Changed

- Update many dependencies.
|Name             | Old version | New version |
|:----------------|:-----------:|:-----------:|
|psycopg          | 3.0.13      | 3.1.4       |
|pytest-sugar     | 0.9.4       | 0.9.5       |
|xdoctest         | 0.15.10     | 1.1.0       |
|pre-commit       | 2.17.0      | 2.20.0      |
|black            | 22.1.0      | 22.10.0     |
|codespell        | 2.1.0       | 2.2.2       |
|dlint            | 0.12.0      | 0.13.0      |
|mypy             | 0.942       | 0.982       |
|pylint           | 2.13.7      | 2.15.5      |
|plerr            | 2.0.0       | 3.0.0       |
|sqlfluff         | 0.12.0      | 1.4.1       |
|flake8           | 3.9         | 5.0.4       |
|flake8-bandit    | 3.0.0       | 4.1.1       |
|flake8-eradicate | 1.2.0       | 1.4.0       |

### Remove

- Remove some depandencies:
  - flakehell
  - flake8-quotes
  - flit
  - m2r2
  - dephell

## [1.0.1] - 2022-05-16

### Added

- Add the function to create a temporary database.

### Fixed

- Fix import in __init__.py
