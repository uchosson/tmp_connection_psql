-- SQL commands to create a temporary database for the main
-- Create table objects
CREATE TABLE objects (id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL);
CREATE INDEX datagin ON objects USING gin(data);
-- Insert into objects
INSERT INTO objects(data) VALUES
('{"obj": {"name": "earth", "mass": 12}}'); -- id = 1
INSERT INTO objects(data) VALUES
('{"obj": {"name": "saturn", "mass": 91}}'); -- id = 2
INSERT INTO objects(data) VALUES
('{"obj": {"name": "jupiter", "mass": 105}}'); -- id = 3
INSERT INTO objects(data) VALUES
('{"obj": {"name": "beta pic ter", "mass": 12}}'); -- id = 4
INSERT INTO objects(data) VALUES
('{"obj": {"name": "beta pic bis", "mass": 26}}'); -- id = 5
INSERT INTO objects(data) VALUES
('{"obj": {"name": "toto", "mass": 3}}'); -- id = 6
INSERT INTO objects(data) VALUES
('{"obj": {"name": "beta pic", "mass": 5}}'); -- id = 7
-- Create table satellite
CREATE TABLE satellite (id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL);
CREATE INDEX datasgin ON satellite USING gin(data);
-- Insert into satellite
INSERT INTO satellite(data) VALUES
('{"obj": {"name": "titi", "owner_id": 6}}'); -- id = 1
