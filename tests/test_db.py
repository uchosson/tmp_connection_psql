# Third party imports
from assertpy import assert_that

# First party imports
from tmp_connection_psql.tmp_connection_psql import tmp_connection


def test_db():
    with tmp_connection("dummypassword", "./tests/test_sql_cmd.sql") as conn:
        cursor = conn.cursor()
        cursor.execute("""SELECT "data"->'obj'->>'name' FROM objects""")
        record = cursor.fetchall()

    assert_that(record).contains_only(
        *[
            ("earth",),
            ("saturn",),
            ("jupiter",),
            ("beta pic ter",),
            ("beta pic bis",),
            ("toto",),
            ("beta pic",),
        ],
    )
