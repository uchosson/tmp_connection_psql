# Remove __pycache__ and .pyc folders and files
clean:
	find . \( -name __pycache__ -o -name "*.pyc" \) -delete

# Clear and display pwd
clear:
	clear
	pwd

# Install no-dev dependencies with poetry
install : clean clear
	poetry install --no-dev --remove-untracked

# install all dependencies with poetry
install-dev: clean clear
	poetry install --remove-untracked

# Launch exodam pytest
test : clean clear
	python3 -m pytest

# Launch test and clean
pytest : test clean

# Do a clean install of pre-commit dependencies
preinstall: clean clear
	pre-commit clean
	pre-commit autoupdate
	pre-commit install --hook-type pre-merge-commit
	pre-commit install --hook-type pre-push
	pre-commit install --hook-type post-rewrite

# Simulate a pre-commit check on added files
prepre : clean clear
	#!/usr/bin/env sh
	set -eux pipefail
	git status
	pre-commit run --all-files

# Launch docstring verification with pydocstyle
pydocstyle : clean clear
	poetry run pydocstyle

# Launch the mypy type linter on the module
mypy : clean clear
	mypy --pretty -p tmp_connection_psql --config-file pyproject.toml

# Run pylint
pylint: clean clear
	pylint --output-format=colorized --msg-template='{msg_id}: in the file: {path}, at line: {line:}, at column: {column}, in objects: {obj} -> {msg}' tmp_connection_psql/

#Run flakehell
flakehell: clean clear
	flakehell lint tmp_connection_psql/

# List of all just commands
list :
	just --list
